const CYCLES = 1000;
const ONLY = process.env.ONLY;
const TEST = 'DI';

const BaseSafe = require('../baseSafe');
const Helper = require('../external-helper');
const flow = require('./config/flow.json');
const baseSafe = new BaseSafe();
const helper = new Helper();
const chai = require('chai');
const expect = chai.expect;

describe('Digital in', async function () {
  this.timeout(CYCLES * 50 + 400*7 + 10000); // 50m cycles + 400*7ms for io setup + 10s for generic setup

  before(async function () {
    if (ONLY && ONLY !== TEST) {
      console.warn(`Skip test, only ${ONLY} selected`);
      return this.skip();
    }
    await helper.init();
    await baseSafe.init();
    await baseSafe.login();
    await baseSafe.saveFlow(flow);
  });

  after(async function () {
    if (ONLY && ONLY !== TEST) {
      return;
    }
    await baseSafe.close();
    await helper.close();
  });

  beforeEach(async () => {
    await helper.clear();
    await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
  });

  it(`should receive pulse signals for DI0 - DI7 ${CYCLES} times within 50ms each pulse`, async () => {
    for (let i = 0; i<= 7; i++) {
      await helper.changeInput(helper.DIGITAL_INPUTS[`DI${i}`], helper.DIGITAL_INPUTMODE.PULSE_COUNT, CYCLES); /// set how many CYCLES
      await helper.changeInput(helper.DIGITAL_INPUTS[`DI${i}`], helper.DIGITAL_INPUTMODE.PULSE); // start pulse
    }
    await helper.sleep(CYCLES*50 + 200); // sleep each cycle 50ms + 200ms to be sure
    for (let i = 0; i<= 7; i++) {
      const val = parseInt(await baseSafe.fetch(`/bench/nodes/tests/DI${i}`), 10);
      expect(val).to.be.equal(CYCLES);
    }
  });

});
