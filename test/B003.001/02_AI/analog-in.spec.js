const MEASURE_ERROR_VOLTAGE_AMPS = 0.05;
const MEASURE_ERROR_RESISTANCE = 400;
const ONLY = process.env.ONLY;
const TEST = 'AI';
var hwConfig = {};

const BaseSafe = require('../baseSafe');
const Helper = require('../external-helper');
const voltageConfig = require('./config/voltage.hw.config.json');
const ampsConfig = require('./config/amps.hw.config.json');
const resistanceConfig = require('./config/resistance.hw.config.json');
const flow = require('./config/flow.json');
const baseSafe = new BaseSafe();
const helper = new Helper();
const chai = require('chai');
const expect = chai.expect;

describe('Analog In', async function () {
  this.timeout(20000);

  before(async function () {
    if (ONLY && ONLY !== TEST) {
      console.warn(`Skip test, only ${ONLY} selected`);
      return this.skip();
    }
    await helper.init();
    await baseSafe.init();
    await baseSafe.login();
    hwConfig = await baseSafe.getControlConf();
    await baseSafe.saveFlow(flow);
  });

  after(async function () {
    if (ONLY && ONLY !== TEST) {
      console.warn(`Skip test, only ${ONLY} selected`);
      return;
    }
    await baseSafe.setControlConf(hwConfig);
    await baseSafe.close();
    await helper.close();
  });

  beforeEach(async () => {
    await helper.clear();
  });

  describe('Voltage', async () => {
    before(async () => {
      await baseSafe.setControlConf(voltageConfig);
    });

    for (let INPUT of ['AI0', 'AI1', 'AI2', 'AI3']) {
      it(`should measure correct voltage on analog input ${INPUT}`, async () => {
        await helper.changeInput(helper.ANALOG_INPUTS[INPUT], helper.ANALOG_INPUTMODES.U);
        await helper.setAnalogInputVoltage(10);
        const ten = parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${INPUT}`));
        await helper.setAnalogInputVoltage(5);
        const five = parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${INPUT}`));
        await helper.setAnalogInputVoltage(1);
        const one = parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${INPUT}`));

        expect(ten).to.be.within(10 - 10*MEASURE_ERROR_VOLTAGE_AMPS, 10 + 10*MEASURE_ERROR_VOLTAGE_AMPS);
        expect(five).to.be.within(5 - 5*MEASURE_ERROR_VOLTAGE_AMPS, 5 + 5*MEASURE_ERROR_VOLTAGE_AMPS);
        expect(one).to.be.within(1 - 1*MEASURE_ERROR_VOLTAGE_AMPS, 1 + 1*MEASURE_ERROR_VOLTAGE_AMPS);
      });
    }
  });

  describe('Amps', async () => {
    before(async () => {
      await baseSafe.setControlConf(ampsConfig);
    });

    for (let INPUT of ['AI0', 'AI1', 'AI2', 'AI3']) {
      it(`should measure correct amps on analog input ${INPUT}`, async () => {
        await helper.changeInput(helper.ANALOG_INPUTS[INPUT], helper.ANALOG_INPUTMODES.I);
        await helper.setAnalogInputAmps(20);
        const twenty = parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${INPUT}`));
        await helper.setAnalogInputAmps(10);
        const ten = parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${INPUT}`));
        await helper.setAnalogInputAmps(5);
        const five = parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${INPUT}`));

        expect(twenty).to.be.within(20-20*MEASURE_ERROR_VOLTAGE_AMPS, 20+20*MEASURE_ERROR_VOLTAGE_AMPS);
        expect(ten).to.be.within(10-10*MEASURE_ERROR_VOLTAGE_AMPS, 10+10*MEASURE_ERROR_VOLTAGE_AMPS);
        expect(five).to.be.within(5-5*MEASURE_ERROR_VOLTAGE_AMPS, 5+5*+MEASURE_ERROR_VOLTAGE_AMPS);
      });
    }
  });

  describe('Resistance', async () => {
    before(async () => {
      await baseSafe.setControlConf(resistanceConfig);
    });

    for (let INPUT of ['AI0', 'AI1', 'AI2', 'AI3']) {
      it(`should measure correct resistance on analog input ${INPUT}`, async () => {
        await helper.changeInput(helper.ANALOG_INPUTS[INPUT], helper.ANALOG_INPUTMODES.R);
        await helper.setAnalogInputResistance(0);
        const zero = parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${INPUT}`));
        await helper.setAnalogInputResistance(1100);
        const tho = parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${INPUT}`));
        await helper.setAnalogInputResistance(2200);
        const twot = parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${INPUT}`));
        await helper.setAnalogInputResistance(Infinity);
        // tester has internal resitance, so disconnect module
        // await helper.clear();
        await helper.changeInput(helper.ANALOG_INPUTS[INPUT], helper.ANALOG_INPUTMODES.OFF);
        const infinity = parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${INPUT}`));
        expect(zero).to.be.within(-MEASURE_ERROR_RESISTANCE, MEASURE_ERROR_RESISTANCE);
        expect(tho).to.be.within(1100-MEASURE_ERROR_RESISTANCE, 1100+MEASURE_ERROR_RESISTANCE);
        expect(twot).to.be.within(2200-MEASURE_ERROR_RESISTANCE, 2200+MEASURE_ERROR_RESISTANCE);
        expect(infinity).to.be.greaterThan(500000);
      });
    }
  });


});
