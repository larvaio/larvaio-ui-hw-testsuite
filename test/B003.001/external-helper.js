const HELPER = process.env.HELPER;
const HELPER_PORT = process.env.HELPER_PORT || 502;
const EventEmitter = require('events');
const ModbusRTU = require('modbus-serial');
const client = new ModbusRTU();

const ANALOG_INPUTMODES = {
  OFF: 0x0,
  R: 0x1,
  U: 0x2,
  I: 0x3
};

const ANALOG_INPUTS = {
  AI0: 0x1,
  AI1: 0x2,
  AI2: 0x3,
  AI3: 0x4,
};

const DIGITAL_INPUTMODE = {
  OFF: 0x0,
  PULSE: 0x1,
  PULSE_COUNT: 0x3,
  FIXED: 0x2,
};

const DIGITAL_INPUTS = {
  DI0: 0xA,
  DI1: 0xB,
  DI2: 0xC,
  DI3: 0xD,
  DI4: 0xE,
  DI5: 0xF,
  DI6: 0x10,
  DI7: 0x11,

  Z0: 0x14,
  Z1: 0x15,
  Z2: 0x16,
  Z3: 0x17,
  Z4: 0x18,
  Z5: 0x19,
  Z6: 0x1A,
};

const DIGITAL_OUTPUTS = {
  DO0: 0x0,
  DO1: 0x1,
  DO2: 0x2,
  DO3: 0x3,
  PGM0: 0x4,
  PGM1: 0x5,
  R0: 0xA,
  R1: 0xB,
  R2: 0xC,
  R3: 0xD,
  R4: 0xE,
  R5: 0xF,
  R6: 0x10,
  R7: 0x11,
};

module.exports = class Helper extends EventEmitter {
  constructor () {
    super();
  }

  get ANALOG_INPUTMODES () {
    return ANALOG_INPUTMODES;
  }
  get DIGITAL_INPUTMODE () {
    return DIGITAL_INPUTMODE;
  }
  get DIGITAL_INPUTS () {
    return DIGITAL_INPUTS;
  }
  get ANALOG_INPUTS () {
    return ANALOG_INPUTS;
  }
  get DIGITAL_OUTPUTS () {
    return DIGITAL_OUTPUTS;
  }

  async init () {
    if (this.helper) {
      throw new Error('Helper allreay opened');
    }
    if (!HELPER) {
      throw new Error('HELPER env variable host missing');
    }
    await client.connectTCP(HELPER, { port: HELPER_PORT });
    client.setID(1);
    this.helper = client;
    await this.clear();
  }

  async close () {
    if (this.helper) {
      await this.helper.writeRegisters(1, [1]); // reboot
      await this.clear();
      await this.helper.close();
      this.helper = undefined;
    }
  }

  async clear () {
    // bell off
    await this.helper.writeRegisters(3, [33, 0x2, 0]);
    // pulses off
    await this.helper.writeRegisters(3, [30, 3, 0]);
    await this.helper.writeRegisters(3, [31, 3, 0]);
    await this.helper.writeRegisters(3, [32, 3, 0]);
    await this.helper.writeRegisters(3, [33, 3, 0]);
    await this.helper.writeRegisters(3, [42, 3, 0]);
    await this.helper.writeRegisters(3, [43, 3, 0]);
    await this.helper.writeRegisters(3, [44, 3, 0]);
    await this.helper.writeRegisters(3, [45, 3, 0]);
    await this.helper.writeRegisters(3, [46, 3, 0]);
    //clear AI
    await this.helper.writeRegisters(1, [0, 0]);
    // clear DI
    for (let key in this.DIGITAL_INPUTS) {
      const output = this.DIGITAL_INPUTS[key];
      await this.helper.writeRegisters(3, [output, this.DIGITAL_INPUTMODE.OFF, 0]);
      await this.sleep();
    }
    await this.helper.writeRegisters(4, [0]);
  }

  async changeInput (input, inputmode, value = 1) {
    if (Object.values(ANALOG_INPUTS).indexOf(input) > -1) {
      return this.changeAnalogInput(input, inputmode, value);
    } else if (Object.values(DIGITAL_INPUTMODE).indexOf(inputmode) > -1) {
      return this.changeDigitalInput(input, inputmode, value);
    }
    throw new Error('Invalid input');
  }


  async changeAnalogInput (input, inputmode, value = 1) {
    if (Object.values(ANALOG_INPUTS).indexOf(input) === -1) {
      throw new Error(`Invalid input ${input}`);
    }
    if (Object.values(ANALOG_INPUTMODES).indexOf(inputmode) === -1) {
      throw new Error(`Invalid input mode ${inputmode}`);
    }
    await this.helper.writeRegisters(3, [40, 0x2, inputmode === ANALOG_INPUTMODES.U]);
    await this.sleep();
    await this.helper.writeRegisters(3, [41, 0x2, inputmode === ANALOG_INPUTMODES.I]);
    await this.sleep();
    await this.helper.writeRegisters(3, [input, inputmode === ANALOG_INPUTMODES.OFF ? 0x0 : 0x2, value]);
    await this.sleep(200);
  }


  async changeDigitalInput (input, inputmode, value = 1) {
    if (Object.values(DIGITAL_INPUTS).indexOf(input) === -1) {
      throw new Error(`Invalid input ${input}`);
    }
    if (Object.values(DIGITAL_INPUTMODE).indexOf(inputmode) === -1) {
      throw new Error(`Invalid input mode ${inputmode}`);
    }
    await this.helper.writeRegisters(3, [input, inputmode, value]);
    await this.sleep(200);
  }


  async setAnalogInputVoltage (voltage) {
    if (voltage > 10 || voltage < 0) {
      throw new Error(`Invalid voltage input ${voltage}`);
    }
    const value = Math.round(65535 * voltage / 10);
    await this.helper.writeRegisters(3, [42, 0x2, value]);
    await this.sleep(200);
  }

  async setAnalogInputAmps (milliAmps) {
    if (milliAmps > 20 || milliAmps < 0) {
      throw new Error(`Invalid milliAmps input ${milliAmps}`);
    }
    const value = Math.round(65535 * milliAmps / 20);
    await this.helper.writeRegisters(3, [43, 0x2, value]);
    await this.sleep(200);
  }

  async setAnalogInputResistance (resistance) {
    if (resistance !== 0 && resistance !== 2200 && resistance !== 1100 && resistance !== Infinity) {
      throw new Error(`Invalid resistance input ${resistance}`);
    }
    const R4 = (resistance > 0 && resistance !== Infinity) ? 1 : 0;
    const R5 = (resistance === 1100) ? 1 : 0;
    const R6 = (resistance === 0) ? 1 : 0;
    await this.helper.writeRegisters(3, [44, 0x2, R4]);
    await this.sleep();
    await this.helper.writeRegisters(3, [45, 0x2, R5]);
    await this.sleep();
    await this.helper.writeRegisters(3, [46, 0x2, R6]);
    await this.sleep(200);
  }

  async setZoneResistance (resistance) {
    if (resistance !== 0 && resistance !== 2200 && resistance !== 1100 && resistance !== Infinity) {
      throw new Error(`Invalid resistance input ${resistance}`);
    }
    const R4 = (resistance > 0 && resistance !== Infinity) ? 1 : 0;
    const R5 = (resistance === 1100) ? 1 : 0;
    const R6 = (resistance === 0) ? 1 : 0;
    await this.helper.writeRegisters(3, [30, 0x2, R4]);
    await this.sleep();
    await this.helper.writeRegisters(3, [31, 0x2, R5]);
    await this.sleep();
    await this.helper.writeRegisters(3, [32, 0x2, R6]);
    await this.sleep(500);
  }

  async pulseZoneResitance (start, pulse, count) {
    if (start !== 2200 && start !== 1100 && start !== 0) {
      throw new Error('Resitance pulse start can be 0, 2200 or 1100');
    }
    if (start !== 2200 && start !== 0 && pulse === Infinity) {
      throw new Error('Resitance Infinity start has to be 2200 or 0');
    }
    if (pulse !== 0 && pulse !== 2200 && pulse !== Infinity) {
      throw new Error('Resitance pulse can be Infinity, 2200 or 0');
    }
    await this.setZoneResistance(start);
    if (pulse === 0 || (pulse === Infinity && start === 0)) {
      await this.helper.writeRegisters(3, [32, 0x3, count]);
      await this.sleep();
      await this.helper.writeRegisters(3, [32, 0x1, 1]);
    }  else if (pulse === Infinity && start === 2200) {
      await this.helper.writeRegisters(3, [30, 0x3, count]);
      await this.sleep();
      await this.helper.writeRegisters(3, [30, 0x1, 1]);
    } else {
      await this.helper.writeRegisters(3, [31, 0x3, count]);
      await this.sleep();
      await this.helper.writeRegisters(3, [31, 0x1, 1]);
    }
    await this.sleep();
  }

  async setBellResitace (resistance) {
    if (resistance !== Infinity && resistance !== 1000) {
      throw new Error('Bell resitance must be Infinity or 1000');
    }
    await this.helper.writeRegisters(3, [33, 0x2, resistance === 1000, 10]);
    await this.sleep();
  }

  async getDigitalOutput (output) {
    if (Object.values(DIGITAL_OUTPUTS).indexOf(output) === -1) {
      throw new Error(`Invalid output ${output}`);
    }
    const value = await this.helper.readDiscreteInputs(output, 1);
    return value.data[0];
  }

  async getBellVoltage () {
    // two 16bit registers combined
    const value = await this.helper.readInputRegisters(0, 2);
    const buffer = Buffer.alloc(4);
    buffer.writeUInt16BE(value.data[0], 2);
    buffer.writeUInt16BE(value.data[1], 0);
    return parseInt(buffer.readInt32BE(), 10) / 1000000; // uV => V;
  }

  async sleep (ms = 55) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
};

