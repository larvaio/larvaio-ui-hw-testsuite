const CYCLES = 100;
const BaseSafe = require('../baseSafe');
const Helper = require('../external-helper');
const flow = require('./config/flow.json');
// const SECURITY_SYSTEM = require('./config/security.system.json');
const EOL_ZONE_TAMPER = require('./config/z_eol_tamper.config.json');
const EOL_ZONE = require('./config/z_eol.config.json');
const TAMPER_ZONE = require('./config/z_tamper.config.json');
const ZONE_NO = require('./config/z.config.json');
const baseSafe = new BaseSafe();
const helper = new Helper();
const chai = require('chai');
const expect = chai.expect;
// const nodeId = flow.find(node => node.type === 'lar-area').id;
const ONLY = process.env.ONLY;
const TEST = 'Z';

describe('Zone', async function () {
  this.timeout(CYCLES * 50 * 4 * 3 + 400*7 + 10000); // 50m cycles + 400*7ms for io setup + 10s for generic setup

  before(function () {
  });

  before(async function () {
    if (ONLY && ONLY !== TEST) {
      console.warn(`Skip test, only ${ONLY} selected`);
      return this.skip();
    }
    await helper.init();
    await baseSafe.init();
    await baseSafe.login();
    await baseSafe.saveFlow([]); // reset flow if security is used
    // await baseSafe.setSecurity(SECURITY_SYSTEM); // we use security system so we have maximum load
    await baseSafe.saveFlow(flow);
  });

  after(async function () {
    if (ONLY && ONLY !== TEST) {
      return;
    }
    /*
    const aremedRes = await baseSafe.request(nodeId, {});
    if (aremedRes.state.armed) {
      await baseSafe.request(nodeId, {
        'command': 'disarm',
      });
    }
    await this.saveFlow([]); // reset flow if security is used
    await this.setSecurity([]); // reset security system
    */
    await baseSafe.close();
    await helper.close();
  });

  beforeEach(async () => {
    await helper.clear();
  });

  for (let ZONE of ['Z0', 'Z1', 'Z2', 'Z3', 'Z4', 'Z5', 'Z6']) {
    // tresholds: 550, 1650, 10000
    it(`Should receive ${CYCLES*3} resitance pulses for ${ZONE} (EOL + TAMPER)`,  async () => {
      await baseSafe.setControlConf(EOL_ZONE_TAMPER); // Zone IO_CHANGE sent if treshold is exceeded
      await helper.changeInput(helper.DIGITAL_INPUTS[ZONE], helper.DIGITAL_INPUTMODE.FIXED);
      await helper.setZoneResistance(1100);
      await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
      await helper.pulseZoneResitance(1100, 2200, CYCLES); // cycle 1k1 vs 2k2
      await helper.sleep(CYCLES*50 + 200); // sleep each cycle 50ms + 200ms to be sure
      expect(parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${ZONE}/count`))).to.be.equal((CYCLES));

      await helper.setZoneResistance(1100);
      await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
      await helper.pulseZoneResitance(1100, 0, CYCLES); // cycle 1k1 vs 0
      await helper.sleep(CYCLES*50 + 200); // sleep each cycle 50ms + 200ms to be sure
      expect(parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${ZONE}/count`))).to.be.equal((CYCLES));

      await helper.setZoneResistance(2200);
      await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
      await helper.pulseZoneResitance(2200, Infinity, CYCLES); // cycle 1k1 vs Infinity
      await helper.sleep(CYCLES*50 + 200); // sleep each cycle 50ms + 200ms to be sure
      expect(parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${ZONE}/count`))).to.be.equal((CYCLES));
    });
  }

  for (let ZONE of ['Z0', 'Z1', 'Z2', 'Z3', 'Z4', 'Z5', 'Z6']) {
    // tresholds: 550, 10000
    it(`Should receive ${CYCLES*2} resitance pulses for ${ZONE}  (EOL)`,  async () => {
      await baseSafe.setControlConf(EOL_ZONE); // Zone IO_CHANGE sent if treshold is exceeded
      await helper.changeInput(helper.DIGITAL_INPUTS[ZONE], helper.DIGITAL_INPUTMODE.FIXED);
      await helper.setZoneResistance(1100);
      await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
      await helper.pulseZoneResitance(1100, 2200, CYCLES); // cycle 1k1 vs 2k2
      await helper.sleep(CYCLES*50 + 200); // sleep each cycle 50ms + 200ms to be sure
      expect(parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${ZONE}/count`))).to.be.equal((0)); // TRESHOLD is not exceeded

      await helper.setZoneResistance(1100);
      await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
      await helper.pulseZoneResitance(1100, 0, CYCLES); // cycle 1k1 vs 0
      await helper.sleep(CYCLES*50 + 200); // sleep each cycle 50ms + 200ms to be sure
      expect(parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${ZONE}/count`))).to.be.equal((CYCLES));

      await helper.setZoneResistance(2200);
      await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
      await helper.pulseZoneResitance(2200, Infinity, CYCLES); // cycle 1k1 vs Infinity
      await helper.sleep(CYCLES*50 + 200); // sleep each cycle 50ms + 200ms to be sure
      expect(parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${ZONE}/count`))).to.be.equal((CYCLES));
    });
  }

  for (let ZONE of ['Z0', 'Z1', 'Z2', 'Z3', 'Z4', 'Z5', 'Z6']) {
    // tresholds: 550, 10000
    it(`Should receive ${CYCLES*2} resitance pulses for ${ZONE}  (TAMPER)`,  async () => {
      await baseSafe.setControlConf(TAMPER_ZONE); // Zone IO_CHANGE sent if treshold is exceeded
      await helper.changeInput(helper.DIGITAL_INPUTS[ZONE], helper.DIGITAL_INPUTMODE.FIXED);
      await helper.setZoneResistance(1100);
      await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
      await helper.pulseZoneResitance(1100, 2200, CYCLES); // cycle 1k1 vs 2k2
      await helper.sleep(CYCLES*50 + 200); // sleep each cycle 50ms + 200ms to be sure
      expect(parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${ZONE}/count`))).to.be.equal((0)); // TRESHOLD is not exceeded

      await helper.setZoneResistance(1100);
      await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
      await helper.pulseZoneResitance(1100, 0, CYCLES); // cycle 1k1 vs 0
      await helper.sleep(CYCLES*50 + 200); // sleep each cycle 50ms + 200ms to be sure
      expect(parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${ZONE}/count`))).to.be.equal((CYCLES));

      await helper.setZoneResistance(2200);
      await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
      await helper.pulseZoneResitance(2200, Infinity, CYCLES); // cycle 1k1 vs Infinity
      await helper.sleep(CYCLES*50 + 200); // sleep each cycle 50ms + 200ms to be sure
      expect(parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${ZONE}/count`))).to.be.equal((CYCLES));
    });
  }

  for (let ZONE of ['Z0', 'Z1', 'Z2', 'Z3', 'Z4', 'Z5', 'Z6']) {
    // tresholds: 10000
    it(`Should receive ${CYCLES} resitance pulses for ${ZONE} (Without EOL and TAMPER)`,  async () => {
      await baseSafe.setControlConf(ZONE_NO); // Zone IO_CHANGE sent if treshold is exceeded
      await helper.changeInput(helper.DIGITAL_INPUTS[ZONE], helper.DIGITAL_INPUTMODE.FIXED);
      await helper.setZoneResistance(1100);
      await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
      await helper.pulseZoneResitance(1100, 2200, CYCLES); // cycle 1k1 vs 2k2
      await helper.sleep(CYCLES*50 + 200); // sleep each cycle 50ms + 200ms to be sure
      expect(parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${ZONE}/count`))).to.be.equal((0)); // TRESHOLD is not exceeded

      await helper.setZoneResistance(1100);
      await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
      await helper.pulseZoneResitance(1100, 0, CYCLES); // cycle 1k1 vs 0
      await helper.sleep(CYCLES*50 + 200); // sleep each cycle 50ms + 200ms to be sure
      expect(parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${ZONE}/count`))).to.be.equal((0)); // TRESHOLD is not exceeded

      await helper.setZoneResistance(0);
      await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
      await helper.pulseZoneResitance(0, Infinity, CYCLES); // cycle 1k1 vs Infinity
      await helper.sleep(CYCLES*50 + 200); // sleep each cycle 50ms + 200ms to be sure
      expect(parseFloat(await baseSafe.fetch(`/bench/nodes/tests/${ZONE}/count`))).to.be.equal((CYCLES));
    });
  }


});
