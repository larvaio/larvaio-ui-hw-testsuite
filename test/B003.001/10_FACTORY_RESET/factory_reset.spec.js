
const ONLY = process.env.ONLY;
const TEST = 'FACTORY';

describe('Factory Reset', async function () {
  before(async function () {
    if (ONLY && ONLY !== TEST) {
      console.warn(`Skip test, only ${ONLY} selected`);
      return this.skip();
    }
  });

  xit('should reset device to defaults', () => {
    // TODO
  });
});
