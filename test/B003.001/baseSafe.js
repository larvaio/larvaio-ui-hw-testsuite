const AdonisBlue = require('../adonis-blue-hw-base');
const DEFAULT_ADMIN_USER = 'admin';
const DEFAULT_ADMIN_PASS = 'admin';
const HOST = process.env.HOST;
const UI_PORT = parseInt(process.env.UI_PORT || 80);

module.exports = class baseSafe extends AdonisBlue {
  constructor () {
    if (!HOST) {
      throw new Error('baseSafe HOST env variable missing');
    }
    super(HOST, UI_PORT);
  }

  async login (username, password) {
    return super.login(username || DEFAULT_ADMIN_USER, password || DEFAULT_ADMIN_PASS);
  }
};
