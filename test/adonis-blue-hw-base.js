/* eslint-disable camelcase */

const REQUEST_TIMEOUT = 5000;

const WebSocket = require('ws');
const uuidv4 = require('uuid/v4');
const fetch = require('node-fetch');

module.exports = class AdonisBlue {
  constructor (host, port = 80, options = {}) {
    if (!host || !(typeof host === 'string')) {
      throw new Error('Host required to connect AdonisBlue based hardware');
    }
    if (!port || !(typeof port === 'number')) {
      throw new Error('Port required to connect AdonisBlue based hardware');
    }
    this.host = host;
    this.options = options;
    this.port = port;
    this.wsProtocol = 'ws';
    this.wsPath = 'ws';
    this.httpProtocol = 'http';
    this.token;
    this.timeouts = [];
  }

  clearTimeouts () {
    for (var i = 0; i < this.timeouts.length; i++) {
      clearTimeout(this.timeouts[i]);
    }
  }

  async init () {
    if (this.client) {
      return this.client;
    }
    const host = `${this.wsProtocol}://${this.host}:${this.port}/${this.wsPath}`;
    this.client = new WebSocket(host, this.options);
    await new Promise((resolve, reject) => {
      this.client.once('error', reject);
      this.client.on('message', msgIn => {
        try {
          const msg = JSON.parse(msgIn);
          if (msg.payload && msg.payload.reqId) {
            this.client.emit(msg.payload.reqId, msg.payload);
          } else if (msg.topic) {
            this.client.emit(msg.topic, msg.payload);
          }
        } catch(err) {
          console.error(err, 'Message: ', msgIn);
          throw err;
        }
      });
      this.client.once('open', () => {
        this.client.removeAllListeners('error');
        this.client.setMaxListeners(0);
        this.client.on('error', err => {
          console.error('Control ERROR:', err);
        });
        resolve();
      });
    });
  }

  async request (cmd, data) {
    if (!this.client) {
      return Promise.reject(new Error('No connection'));
    }
    return new Promise((resolve, reject) => {
      const reqId = uuidv4();
      const token = this.token ? this.token : undefined;
      const msg = {
        topic: `iot-2/cmd/${cmd}/fmt/json`,
        payload:{
          token,
          reqId,
          data
        }
      };
      const timeout = setTimeout(() => {
        this.client.removeAllListeners(reqId);
        let err = new Error(
            `Base did not respond to cmd '${cmd}' reqId '${reqId}
            in time, timeout of ${REQUEST_TIMEOUT}ms occurred.`
        );
        reject(err);
      }, REQUEST_TIMEOUT);
      this.timeouts.push(timeout);
      this.client.once(reqId, (payload) => {
        clearTimeout(timeout);
        if (payload.err) {
          return reject(new Error(payload.err.message));
        }
        resolve(payload.data);
      });
      this.client.send(JSON.stringify(msg));
    });
  }

  async login (username, password) {
    const { access_token } = await this.request('login', { username , password });
    if (access_token) {
      this.token = access_token;
      return true;
    }
    throw new Error('Unknown login error');
  }

  async getLogs (data = {}) {
    return  await this.request('getLogs', data);
  }
  async getControlConf (data = {}) {
    return  await this.request('getHwInfo', data);
  }
  async setControlConf (data) {
    return  await this.request('setControlConf', data);
  }
  async setSecurity (data) {
    return  await this.request('setSecurity', data);
  }

  async saveFlow (flows) {
    if (!this.token) {
      throw new Error('Not logged in to Adonis Based controller');
    }
    const headers = {
      'Content-Type': 'application/json',
      'Node-RED-API-Version': 'v2',
      'Node-RED-Deployment-Type': 'full',
    };
    const path = `/bench/editor/flows`;
    const currentFlows = await this.fetch(path, { headers });
    const data = {
      flows,
      rev: currentFlows.rev // @see https://nodered.org/docs/api/admin/methods/post/flows/
    };
    const saveData = await this.fetch(path, {
      method: 'post',
      body: JSON.stringify(data),
      headers,
    });
    if (!saveData.rev) {
      throw new Error('Unknown error while saving flow');
    }
    // cbeck that flow deployed and system is running
    let started = false;
    let i = 0;
    while (started === false) {
      try {
        await this.fetch(path, { headers }); // throws if "system is starting"
        started = true;
      } catch (err) {
        // supress
        if (i === 4) {
          throw new Error('Flow did not start...' + err.message);      
        }
        await this.sleep(200);
        i++;
      }
    }
  }
  
  async sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async fetch (path, options = {}) {
    const url = `${this.httpProtocol}://${this.host}:${this.port}${path}`;
    if (!options.headers) {
      options.headers = {};
    }
    if (this.token) {
      options.headers = {
        ...options.headers,
        //'Authorization': `Bearer ${this.token}`
        'Cookie': `token=${this.token}`
      };
    }
    const res = await fetch(url, options);
    const body = await res.text();
    try {
    const json = JSON.parse(body);
    return json;
    } catch (err) {
      console.error(`Invalid JSON message received, expected JSON`, body);
      throw new Error(`Invalid JSON message received, expected JSON for request ${path}`);
    }
  }

  close () {
    if (this.client) {
      this.client.removeAllListeners();
      this.client.close();
      this.client = undefined;
    }
    this.clearTimeouts();
  }
};
