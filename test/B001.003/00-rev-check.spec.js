const REV = 'B001.003';
const REV_TOTEST = process.env.REV;
before(function () {
  if (REV !== REV_TOTEST) {
    console.warn(`Skip hardware revision ${REV} tests, curretly testing hardware revision ${REV_TOTEST}`);
    return this.skip();
  }
});
