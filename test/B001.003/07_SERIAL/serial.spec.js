
const ONLY = process.env.ONLY;
const TEST = 'SERIAL';
const BaseSafe = require('../baseSafe');
const flow1 = require('./config/flow1.json');
const flow2 = require('./config/flow2.json');
const baseSafe = new BaseSafe();
const chai = require('chai');
const expect = chai.expect;

describe('Serial', async function () {
  this.timeout(10000);

  before(async function () {
    if (ONLY && ONLY !== TEST) {
      console.warn(`Skip test, only ${ONLY} selected`);
      return this.skip();
    }
    await baseSafe.init();
    await baseSafe.login();
  });

  after(async function () {
    if (ONLY && ONLY !== TEST) {
      return;
    }
    await baseSafe.close();
  });

  it('Should test serial one direction', async () => {
    await baseSafe.saveFlow(flow1);
    const random = Math.random().toString(36).substring(7);
    await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
    await baseSafe.fetch(`/bench/nodes/tests/serial`,{
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ data: random + '\n' }),
    });
    await new Promise(resolve => setTimeout(resolve, 200));
    const res = await baseSafe.fetch(`/bench/nodes/tests/serial`);
    expect(res.data).to.be.equal(random);
  });

  it('Should test serial second direction', async () => {
    await baseSafe.saveFlow(flow2);
    const random = Math.random().toString(36).substring(7);
    await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
    await baseSafe.fetch(`/bench/nodes/tests/serial`,{
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ data: random + '\n' }),
    });
    await new Promise(resolve => setTimeout(resolve, 200));
    const res = await baseSafe.fetch(`/bench/nodes/tests/serial`);
    expect(res.data).to.be.equal(random);
  });
});

