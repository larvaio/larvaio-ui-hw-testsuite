const BaseSafe = require('../baseSafe');
const Helper = require('../external-helper');
const flow = require('./config/flow.json');
const baseSafe = new BaseSafe();
const helper = new Helper();
const chai = require('chai');
const expect = chai.expect;
const ONLY = process.env.ONLY;
const TEST = 'BELL';


describe('Bell', async function () {

  this.timeout(40000); // bell check intervall is 10sec

  before(async function () {
    if (ONLY && ONLY !== TEST) {
      console.warn(`Skip test, only ${ONLY} selected`);
      return this.skip();
    }
    await helper.init();
    await baseSafe.init();
    await baseSafe.login();
    await baseSafe.saveFlow(flow);
  });

  after(async function () {
    if (ONLY && ONLY !== TEST) {
      return;
    }
    await baseSafe.close();
    await helper.close();
  });

  beforeEach(async () => {
    await helper.clear();
  });

  it('should change bell resitance value', async () => {
    await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow counters
    await helper.setBellResitace(1000);
    await helper.sleep(11000); // bell check intervall is 10sec
    await helper.setBellResitace(Infinity);
    await helper.sleep(11000); // bell check intervall is 10sec
    expect(parseFloat(await baseSafe.fetch(`/bench/nodes/tests/BELL0/value`))).to.be.below(1.5); // BELL input is measured using Voltage and voltage is reversed
    await helper.setBellResitace(1000);
    await helper.sleep(11000); // bell check intervall is 10sec
    expect(parseFloat(await baseSafe.fetch(`/bench/nodes/tests/BELL0/value`))).to.be.above(1.5); // BELL input is measured using Voltage and voltage is reversed
  });
});
