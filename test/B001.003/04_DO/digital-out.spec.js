const BaseSafe = require('../baseSafe');
const Helper = require('../external-helper');
const flow = require('./config/flow.json');
const baseSafe = new BaseSafe();
const helper = new Helper();
const chai = require('chai');
const expect = chai.expect;
const ONLY = process.env.ONLY;
const TEST = 'DO';

describe('Digital out', async function () {
  this.timeout(20000);

  before(async function () {
    if (ONLY && ONLY !== TEST) {
      console.warn(`Skip test, only ${ONLY} selected`);
      return this.skip();
    }
    await helper.init();
    await baseSafe.init();
    await baseSafe.login();
    await baseSafe.saveFlow(flow);
  });

  after(async function () {
    if (ONLY && ONLY !== TEST) {
      return;
    }
    await baseSafe.close();
    await helper.close();
  });

  beforeEach(async () => {
    await helper.clear();
    await baseSafe.fetch(`/bench/nodes/resettests`); // resets flow outputs
  });

  for (let OUTPUT of ['DO0', 'DO1', 'DO2', 'DO3', 'R0', 'R1', 'R2', 'R3', 'R4', 'R5', 'R6', 'R7']) {
    it(`Sould toggle ${OUTPUT}`, async () => {
      expect(await helper.getDigitalOutput(helper.DIGITAL_OUTPUTS[OUTPUT])).to.be.equal(false);
      await baseSafe.fetch(`/bench/nodes/tests/${OUTPUT}/true`);
      expect(await helper.getDigitalOutput(helper.DIGITAL_OUTPUTS[OUTPUT])).to.be.equal(true);
      await baseSafe.fetch(`/bench/nodes/tests/${OUTPUT}/false`);
      expect(await helper.getDigitalOutput(helper.DIGITAL_OUTPUTS[OUTPUT])).to.be.equal(false);
    });
  }
});
