const BaseSafe = require('../baseSafe');
const Helper = require('../external-helper');
const flow = require('./config/flow.json');
const SECURITY_SYSTEM = require('./config/security.system.json');
const ZONE_FIXED = require('./config/zone.config.json');
const baseSafe = new BaseSafe();
const helper = new Helper();
const chai = require('chai');
const expect = chai.expect;
const nodeId = flow.find(node => node.type === 'lar-area').id;
const ONLY = process.env.ONLY;
const TEST = 'SECURITY';

describe('Security', async function () { // tests that security system receives Zone inputs and controls bell output
  this.timeout(40000); // bell timeout

  before(async function () {
    if (ONLY && ONLY !== TEST) {
      console.warn(`Skip test, only ${ONLY} selected`);
      return this.skip();
    }
    await helper.init();
    await baseSafe.init();
    await baseSafe.login();
    await baseSafe.saveFlow([]); // reset flow if security is used
    await baseSafe.setControlConf(ZONE_FIXED);
    await baseSafe.setSecurity([]);
    await baseSafe.setSecurity(SECURITY_SYSTEM);
    await baseSafe.saveFlow(flow);
  });

  after(async function () {
    if (ONLY && ONLY !== TEST) {
      return;
    }
    const aremedRes = await baseSafe.request(nodeId, {});
    if (aremedRes.state.armed) {
      await baseSafe.request(nodeId, {
        'command': 'disarm',
      });
    }
    await baseSafe.saveFlow([]); // reset flow if security is used
    await baseSafe.setSecurity([]); // reset security system
    await baseSafe.close();
    await helper.close();
  });

  beforeEach(async () => {
    await helper.clear();
  });

  for (let ZONE of ['Z0', 'Z1', 'Z2', 'Z3', 'Z4', 'Z5', 'Z6']) {
    it(`should arm area and trigger alarm with ${ZONE} if zone opened 50ms`, async () => {
      await baseSafe.request(nodeId, {
        'command': 'arm',
        'override_zones': [],
        'override_troubles': true
      });
      await helper.sleep(1100); // 1sec exit delay
      const aremedRes = await baseSafe.request(nodeId, {});
      expect(aremedRes.state.armed).to.be.equal(true);
      await helper.changeInput(helper.DIGITAL_INPUTS[ZONE], helper.DIGITAL_INPUTMODE.FIXED);
      await helper.setZoneResistance(Infinity);
      await helper.helper.writeRegisters(3, [32, 0x3, 2]);
      await helper.sleep(); // 50ms
      await helper.helper.writeRegisters(3, [32, 0x1, 1]);
      await helper.sleep(500); // .5s to be sure
      const alarmRes = await baseSafe.request(nodeId, {});
      await baseSafe.request(nodeId, {
        'command': 'disarm',
      });
      expect(alarmRes.state.alarm).to.be.equal(true);
    });
  }

  it(`should create and resolve bell issue`, async () => {
    await helper.setBellResitace(Infinity);
    await helper.sleep(11000); // bell check intervall is 10sec
    let troblesRes = await baseSafe.request(nodeId, {});
    let troubles = troblesRes.state.troubles || [];
    let bellTrouble = troubles.find(el => el.type_id === 8 && el.resolved === undefined) || {};
    expect(bellTrouble.type_id).to.be.equal(8);
    await helper.setBellResitace(1000);
    await helper.sleep(11000); // bell check intervall is 10sec
    troblesRes = await baseSafe.request(nodeId, {});
    troubles = troblesRes.state.troubles || [];
    bellTrouble = troubles.find(el => el.type_id === 8 && el.resolved !== undefined) || {};
    expect(bellTrouble.type_id).to.be.equal(8);
    await helper.setBellResitace(Infinity);
  });

  it(`should trigger 12V to bell output`, async () => {
    await helper.setBellResitace(1000); // to reduce leakage voltage
    const aremedRes = await baseSafe.request(nodeId, {});
    if (aremedRes.state.armed) {
      await baseSafe.request(nodeId, {
        'command': 'disarm',
      });
    }
    await helper.changeInput(helper.DIGITAL_INPUTS.Z0, helper.DIGITAL_INPUTMODE.FIXED);
    await helper.setZoneResistance(Infinity);
    await helper.sleep(1000); // beckhoff multimeter 0.5sec delay
    expect(await helper.getBellVoltage()).to.be.within(0, 1);
    await baseSafe.request(nodeId, {
      'command': 'arm',
      'override_zones': [],
      'override_troubles': true
    });
    await helper.sleep(1000); // 1sec exit delay
    await helper.setZoneResistance(0); // trigger alarm
    await helper.sleep(1000); // beckhoff multimeter 0.5sec delay + larva delay
    expect(await helper.getBellVoltage()).to.be.within(10, 14);
    await baseSafe.request(nodeId, {
      'command': 'disarm',
    });
  });
});
