
# Tester suite for Larva Base

## System requirements

* allowed to attach host netwrok to container
* docker  + docker-compose

## Usage

```bash
REV=B001.003 HOST=<target device address> HELPER=<helper device address> docker-compose up
docker-compose down
```

Optionally you can use ONLY env variable to test only specific set of tests

* AI
* DI
* DO
* Z
* DISCOVERY
* SERIAL
* BELL
* SECURITY
* FACTORY

Fo example:

```bash
(HOST=192.168.114.100 PORT=80 REV=B003.001 HELPER=192.168.114.22 \
    docker-compose up --exit-code-from tester --abort-on-container-exit || echo -e "\e[0;31;1mTESTS FAILED\e[m"; ) && \
    docker-compose down 2>/dev/null

```
